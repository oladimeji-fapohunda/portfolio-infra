#
# Portfolio_Infra-terraform

The code snippet is a Terraform configuration that sets up an AWS provider and creates an EC2 instance using a module.

### Example Usage
```terraform
provider "aws" {
  region = "us-east-1"
}

module "ec2-web" {
  source = "./modules/ec2"
}
```

### Full Explanation
The code snippet is a Terraform configuration written in HashiCorp Configuration Language (HCL). It sets up an AWS provider and creates an EC2 instance using a module.

The `provider` block specifies the AWS provider and sets the region to "us-east-1". This means that any resources created in this configuration will be provisioned in the US East (N. Virginia) region.

The `module` block defines the creation of an EC2 instance using a module located in the "./modules/ec2" directory. The `source` parameter specifies the path to the module. This module likely contains the necessary configuration to create an EC2 instance, such as the instance type, security groups, and other settings.

Overall, this code snippet is a standalone Terraform configuration that sets up an AWS provider and creates an EC2 instance using a module. It is not part of a larger function or class.
#


# Portfolio_infra-Ansible

The given Ansible code snippet is a part of a larger Ansible playbook. It sets up a web server by executing several roles, including `base-server-setup`, `git`, `apache`, and `firewalld`.

### Example Usage
```ansible
- name: Web server
  hosts: web
  become: true
  gather_facts: true

  roles: 
    - base-server-setup
    - git
    - apache
    - firewalld
```

### Full Explanation
The Ansible playbook that configures the web server. Here is a breakdown of the different components:

- `name`: Specifies the name of the playbook, which is "Web server".
- `hosts`: Defines the target hosts on which the playbook will be executed. In this case, it is set to "web", indicating that the playbook will be applied to the hosts defined in the "web" group.
- `become`: Enables privilege escalation, allowing the playbook to run with administrative privileges.
- `gather_facts`: Specifies whether Ansible should gather facts about the target hosts before executing tasks. Setting it to "true" enables gathering facts.
- `roles`: Specifies the roles that will be executed as part of the playbook. Roles are reusable and modular units of configuration that can be applied to multiple hosts. In this case, the playbook executes the roles `base-server-setup`, `git`, `apache`, and `firewalld` in that order.



