module "ec2-web" {
  source                 = "terraform-aws-modules/ec2-instance/aws"

  ami                    = "ami-08a52ddb321b32a8c"
  instance_type          = "t2.micro"
  key_name               = "main-key"
  monitoring             = true
  vpc_security_group_ids = ["sg-0713dd723f9da6f00"]
  subnet_id              = "subnet-0c09c1f88ce54e26a"
  user_data              = "${file("./files/user-data")}"

  tags = {
    Name        = "portfolio-web"
    Terraform   = "true"
    Environment = "dev"
  }
}