output "public_ip" {
    description = "The public ip of the instance"
    value = module.ec2-web.public_ip
}